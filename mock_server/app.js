var express = require('express')
var app = express()

const PARAMETER_VALUES = {
  ip_mode: '1',
  ip_address: '1',
  subnet_mask: '1',
  gateway: '1',
  mac_address: '1',
  ip_mode_current: '1',
  ip_address_current: '1',
  subnet_mask_current: '1',
  gateway_current: '1',
  serial: '1',
  vendor: '1',
  product: '1',
  part: '1',
  revision_fw: '1',
  revision_hw: '1',
  feature_flags: '1',
  emitter_type: '1',
  device_family: '1',
  max_connections: '1',
  scan_frequency_min: '1',
  scan_frequency_max: '1',
  radial_range_min: '1',
  radial_range_max: '1',
  radial_resolution: '1',
  angular_resolution: '1',
  angular_fov: '1',
  sampling_rate_min: '1',
  sampling_rate_max: '1',
  up_time: '1',
  power_cycles: '1',
  operation_time: '1',
  operation_time_scaled: '1',
  temperature_current: '1',
  temperature_min: '1',
  temperature_max: '1',
  system_time_raw: '1',
  status_flags: '1',
  scan_frequency_measured: '1',
  locator_indication: '1',
  pilot_laser: '1',
  pilot_start_angle: '1',
  pilot_stop_angle: '1',
  user_tag: '1',
  layer_enable: '1',
  scan_frequency: '1',
  scan_direction: '1',
  measure_start_angle: '1',
  measure_stop_angle: '1',
  operating_mode: '1',
  samples_per_sca: '1'
}

app.get('/cmd/get_parameter', (req, res, next) => {
  const parameters = Object.keys(req.query)

  res.header('Access-Control-Allow-Origin', req.headers.origin || '*')
  res.json(
    parameters.reduce(
      (acc, x) => {
        acc[x] = PARAMETER_VALUES[x]
        return acc
      },
      {error_code: 0, error_text: 'success'}
    )
  )
})

app.get('/cmd/set_parameter', (req, res, next) => {
  Object.entries(req.query).map(
    ([key, value]) => (PARAMETER_VALUES[key] = value)
  )

  res.header('Access-Control-Allow-Origin', req.headers.origin || '*')
  res.json({error_code: 0, error_text: 'success'})
})

app.listen(3000, () => {
  console.log('Server is listening on port 3000')
})
