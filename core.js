const HOST = 'localhost:3000'
const PARAMETERS = [
  'ip_mode',
  'ip_address',
  'subnet_mask',
  'gateway',
  'mac_address',
  'ip_mode_current',
  'ip_address_current',
  'subnet_mask_current',
  'gateway_current',
  'serial',
  'vendor',
  'product',
  'part',
  'revision_fw',
  'revision_hw',
  'feature_flags',
  'emitter_type',
  'device_family',
  'max_connections',
  'scan_frequency_min',
  'scan_frequency_max',
  'radial_range_min',
  'radial_range_max',
  'radial_resolution',
  'angular_resolution',
  'angular_fov',
  'sampling_rate_min',
  'sampling_rate_max',
  'up_time',
  'power_cycles',
  'operation_time',
  'operation_time_scaled',
  'temperature_current',
  'temperature_min',
  'temperature_max',
  'system_time_raw',
  'status_flags',
  'scan_frequency_measured',
  'locator_indication',
  'pilot_laser',
  'pilot_start_angle',
  'pilot_stop_angle',
  'user_tag',
  'layer_enable',
  'scan_frequency',
  'scan_direction',
  'measure_start_angle',
  'measure_stop_angle',
  'operating_mode',
  'samples_per_scan'
]

function addInput([key, value]) {
  const inputHTML = $(
    `<label>${key}</label> <input type="text" name="${key}" id="${key}" value="${value}"/>`
  )
  $('#parametersWrapper').append(inputHTML)
}

function fillParameterList(parameters) {
  Object.entries(parameters).map(addInput)
}

function setPage(parameters) {
  const paramStr = PARAMETERS.map(x => `${x}&`).join('')
  $.ajax({
    type: 'GET',
    url: `http://${HOST}/cmd/get_parameter?${paramStr}`,
    success: data => {
      delete data.error_code
      delete data.error_text
      fillParameterList(data)
    },
    error: error => console.log(error)
  })
}

function submitForm() {
  const data = {}
  $('#parametersWrapper')
    .children('input')
    .each(function() {
      data[this.name] = this.value
    })
  const paramStr = Object.entries(data)
    .map(([key, value]) => `${key}=${value}&`)
    .join('')
  console.log(paramStr)
  $.ajax({
    type: 'GET',
    url: `http://${HOST}/cmd/set_parameter?${paramStr}`,
    success: function(data) {
      console.log(data)
    },
    error: error => console.log(error)
  })
  window.location.reload()
}

$(document).ready(function() {
  setPage()
})
